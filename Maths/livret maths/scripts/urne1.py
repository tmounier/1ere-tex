from random import *
def urne(n):
    """Tire n boules avec remise dans une urne
    contenant 12 boules, 1 noire, 1 blanche
    et 10 rouges"""
    tirages=[]
    for i in range (n):
        boule=randint(1,12)
        if boule ==1:
            tirages.append("B")
        elif boule ==12:
            tirages.append("N")
        else:
            tirages.append("R")
    return(tirages)