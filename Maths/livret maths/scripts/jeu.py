from random import *

def jeu():
    nombre = randint(1,10)
    proposition = 0
    coups = 0
    while proposition != nombre:
        proposition=float(input("Entrez un nombre : "))
        coups += 1
    
    return ("Victoire en {} coups".format(coups))